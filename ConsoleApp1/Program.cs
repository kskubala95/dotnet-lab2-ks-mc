﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication5
{
    class Program
    {
        char[,] Tablica = new char[10, 10];
        void zerujTablice()
        {
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    Tablica[i, j] = '.';
                }
            }
        }

        Program()
        {
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    Tablica[i, j] = '.';
                }
            }
            Tablica[2, 2] = '*';
            Tablica[2, 3] = '*';
            Tablica[2, 4] = '*';
            Tablica[3, 2] = '*';
            Tablica[3, 3] = '*';
            Tablica[3, 4] = '*';
            Tablica[4, 2] = '*';
            Tablica[4, 3] = '*';
            Tablica[4, 4] = '*';
            /*Tablica[3, 0] = '.';
            Tablica[1, 2] = '.';
            Tablica[0, 3] = '.';
            Tablica[7, 1] = '.';
            Tablica[3, 5] = '.';
            Tablica[8, 0] = '.';
            Tablica[1, 6] = '.';
            Tablica[1, 4] = '.';
            Tablica[8, 0] = '.';
            Tablica[4, 5] = '.';
            Tablica[3, 9] = '.';
            Tablica[6, 9] = '.';
            Tablica[7, 5] = '.';
            Tablica[0, 0] = '.';
            Tablica[3, 9] = '.';
            Tablica[3, 9] = '.';
            Tablica[3, 0] = '.';
            Tablica[2, 9] = '.';
            Tablica[5, 0] = '.';
            Tablica[3, 0] = '.';
            Tablica[4, 3] = '.';
            Tablica[5, 1] = '.';
            Tablica[8, 1] = '.';*/
        }
        int SprawdzIloscZywychSasiadow(int x, int y)
        {
            int licznik = 0;

            switch (x)
            {
                case 0:
                    switch (y)
                    {
                        case 0:
                            if (Tablica[x + 1, y] == '*') licznik++;
                            if (Tablica[x + 1, y + 1] == '*') licznik++;
                            if (Tablica[x, y + 1] == '*') licznik++;
                            break;
                        case 9:
                            if (Tablica[x, y - 1] == '*') licznik++;
                            if (Tablica[x + 1, y - 1] == '*') licznik++;
                            if (Tablica[x + 1, y] == '*') licznik++;
                            break;
                        default:
                            if (Tablica[x, y - 1] == '*') licznik++;
                            if (Tablica[x + 1, y - 1] == '*') licznik++;
                            if (Tablica[x + 1, y] == '*') licznik++;
                            if (Tablica[x + 1, y + 1] == '*') licznik++;
                            if (Tablica[x, y + 1] == '*') licznik++;
                            break;
                    }
                    break;

                case 9:
                    switch (y)
                    {
                        case 0:
                            if (Tablica[x - 1, y] == '*') licznik++;
                            if (Tablica[x - 1, y + 1] == '*') licznik++;
                            if (Tablica[x, y + 1] == '*') licznik++;
                            break;
                        case 9:
                            if (Tablica[x - 1, y] == '*') licznik++;
                            if (Tablica[x - 1, y - 1] == '*') licznik++;
                            if (Tablica[x, y - 1] == '*') licznik++;
                            break;
                        default:
                            if (Tablica[x, y - 1] == '*') licznik++;
                            if (Tablica[x - 1, y - 1] == '*') licznik++;
                            if (Tablica[x - 1, y] == '*') licznik++;
                            if (Tablica[x - 1, y + 1] == '*') licznik++;
                            if (Tablica[x, y + 1] == '*') licznik++;
                            break;
                    }
                    break;
                default:
                    switch (y)
                    {
                        case 0:
                            if (Tablica[x - 1, y] == '*') licznik++;
                            if (Tablica[x - 1, y + 1] == '*') licznik++;
                            if (Tablica[x, y + 1] == '*') licznik++;
                            if (Tablica[x + 1, y + 1] == '*') licznik++;
                            if (Tablica[x + 1, y] == '*') licznik++;
                            break;
                        case 9:
                            if (Tablica[x - 1, y] == '*') licznik++;
                            if (Tablica[x - 1, y - 1] == '*') licznik++;
                            if (Tablica[x, y - 1] == '*') licznik++;
                            if (Tablica[x + 1, y - 1] == '*') licznik++;
                            if (Tablica[x + 1, y] == '*') licznik++;
                            break;
                        default:
                            if (Tablica[x - 1, y - 1] == '*') licznik++;
                            if (Tablica[x - 1, y] == '*') licznik++;
                            if (Tablica[x - 1, y + 1] == '*') licznik++;
                            if (Tablica[x, y - 1] == '*') licznik++;
                            if (Tablica[x, y + 1] == '*') licznik++;
                            if (Tablica[x + 1, y - 1] == '*') licznik++;
                            if (Tablica[x + 1, y] == '*') licznik++;
                            if (Tablica[x + 1, y + 1] == '*') licznik++;
                            break;
                    }
                    break;
            }
            return licznik;
        }

        void wyswietlTablice()
        {
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    System.Console.Write(Tablica[i, j]);
                }
                System.Console.WriteLine("");
            }
        }
        static void Main(string[] args)
        {
            Program prog = new Program();
            Program prog2 = new Program();
            prog2.zerujTablice();
            while (true)
            {
                prog.wyswietlTablice();

                for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        if (prog.Tablica[i, j] == '*')
                            if (prog.SprawdzIloscZywychSasiadow(i, j) < 2)
                            {
                                prog2.Tablica[i, j] = '.';
                                System.Console.WriteLine(prog.SprawdzIloscZywychSasiadow(i, j));
                            }
                        if (prog.Tablica[i, j] == '*')
                            if (prog.SprawdzIloscZywychSasiadow(i, j) == 2 || prog.SprawdzIloscZywychSasiadow(i, j) == 3)
                            {
                                prog2.Tablica[i, j] = '*';
                                System.Console.WriteLine(prog.SprawdzIloscZywychSasiadow(i, j));
                            }
                        if (prog.Tablica[i, j] == '*')
                            if (prog.SprawdzIloscZywychSasiadow(i, j) > 3)
                            {
                                prog2.Tablica[i, j] = '.';
                                System.Console.WriteLine(prog.SprawdzIloscZywychSasiadow(i, j));
                            }
                        if (prog.Tablica[i, j] == '.')
                            if (prog.SprawdzIloscZywychSasiadow(i, j) == 3)
                            {
                                prog2.Tablica[i, j] = '*';
                            }
                    }
                }
                prog2.wyswietlTablice();
                Console.ReadKey();
                prog = prog2;
            }
        }
    }
}